package com.stacksimplify.restservices.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@GetMapping("/message")
	public String helloMessage() {
		return "Hi There!!" ; 
	}

	@GetMapping("/userInfo")
	public User userInfo() {
		return new User("Arnav", "Champ", "Bangalore");
	}
	
}
